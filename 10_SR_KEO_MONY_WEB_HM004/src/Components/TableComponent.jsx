import React, { Component } from 'react'
import Swal from 'sweetalert2';

export default class TableComponent extends Component {

    showAlert = (item) => {
        // if 
        Swal.fire({
            title: "ID : " + item.id + '<br> Email : ' + item.email + '<br> Username : ' + item.username + '<br> Age : ' + item.age,
            icon: "success",
            // timer : 3000,
            confirmButtonText: "OK",
        });
    }

    render() {
        return (
            <div>
                <div class="w-4/5 m-auto relative overflow-x-auto mt-20 mb-36">
                    <table class="w-full text-md text-left text-gray-500 dark:text-gray-400 rounded-md overflow-hidden ">
                        <thead class="text-md text-white uppercase bg-blue-400">
                            <tr class="text-center">
                                <th scope="col" class="px-6 py-5">
                                    ID
                                </th>
                                <th scope="col" class="px-6 py-5">
                                    Email
                                </th>
                                <th scope="col" class="px-6 py-5">
                                    Username
                                </th>
                                <th scope="col" class="px-6 py-5">
                                    Age
                                </th>
                                <th scope="col" class="px-6 py-5">
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            {this.props.data.map((item) => (
                                <tr class="text-slate-700 text-sm odd:bg-white even:bg-red-300">
                                    <td class="px-6 py-3">{item.id}</td>
                                    <td class="px-6 py-3">{item.email === "" ? "null" : item.email}</td>
                                    <td class="px-6 py-3">{item.username === "" ? "null" : item.username}</td>
                                    <td class="px-6 py-3">{item.age === "" ? "null" : item.age}</td>
                                    <td class="w-full px-6 py-3 text-white flex">
                                        <button class='w-1/2 bg-red-800 hover:bg-emerald-500 px-7 py-2 mr-3 rounded-md' onClick={() => this.props.changeText(item.id)} >{item.action}</button>
                                        <button class='w-1/2 bg-blue-800 hover:bg-cyan-600 px-7 py-2 rounded-md' onClick={() => this.showAlert(item)}>Show More</button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}
