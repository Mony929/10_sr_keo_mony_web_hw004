import React, { Component } from 'react'
import TableComponent from './TableComponent'


export default class InputComponent extends Component {

    constructor() {
        super()
        this.state = {
            userInfo: [
                { id: 1, username: 'Dara', email: 'keomony929@gmail.com', age: 10, action: "Pending" },
                { id: 2, username: 'Sok', email: 'mony929@gmail.com', age: 20, action: "Pending" },
                { id: 3, username: 'Sokha', email: 'mony29@gmail.com', age: 30, action: "Pending" },
            ],
            newEmail: "",
            newUsername: "",
            newAge: "",
        }
    }

    onChangeEmail = (event) => {
        // console.log(event.target.value);
        this.setState({
            newEmail: event.target.value
        })
    }
    onChangeUsername = (event) => {
        // console.log(event.target.value);
        this.setState({
            newUsername: event.target.value
        })
    }
    onChangeAge = (event) => {
        this.setState({
            newAge: event.target.value
        })
    }

    changeText = (e) => {
        this.state.userInfo.map(getAction => {
            if (e === getAction.id) {
                if (getAction.action === "Pending") {
                    getAction.action = "Done"
                } else {
                    getAction.action = "Pending"
                }
            }
        })
        this.setState({
            getAction: this.state.userInfo
        })
    }

    onRegister = () => {
        const newUser = {
            id: this.state.userInfo.length + 1, username: this.state.newUsername, email: this.state.newEmail, age: this.state.newAge, action: "Pending"
        }
        this.setState({
            userInfo: [...this.state.userInfo, newUser]
        })
    }

    render() {
        return (
            <div class="bg-gradient-to-b from-indigo-400 pt-16">
                {/* information label */}
                <div class="text-5xl font-extrabold">
                    <span class="bg-clip-text text-transparent bg-gradient-to-r from-violet-500 to-pink-500">
                        Please fill your information
                    </span>
                </div>

                {/* form input */}
                <div class="mb-4 mt-12 w-4/5 m-auto">
                    <label className="block text-left text-gray-700 text-lg font-medium mb-2" for="username">
                        Your Email
                    </label>
                    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                        id="username" type="text"
                        placeholder="email@gmail.com"
                        onChange={this.onChangeEmail}
                    />
                </div>
                <div class="mb-4 mt-6 w-4/5 m-auto">
                    <label className="block text-left text-gray-700 text-lg font-medium mb-2" for="username">
                        Username
                    </label>

                    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                        id="username" type="text"
                        placeholder="Keo Mony"
                        onChange={this.onChangeUsername}
                    />
                </div>
                <div class="mb-4 mt-6 w-4/5 m-auto">
                    <label className="block text-left text-gray-700 text-lg font-medium mb-2" for="username">
                        Age
                    </label>

                    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                        id="age" type="text"
                        placeholder="10"
                        onChange={this.onChangeAge}
                    />
                </div>

                {/* button */}
                <button onClick={this.onRegister} type="button" class="mt-4 bg-gradient-to-r from-green-400 to-blue-500 hover:from-pink-500 hover:to-yellow-500 text-white font-bold py-2 px-28 rounded-lg">
                    Register
                </button>
                {/* Table for dispay student name */}
                <TableComponent data={this.state.userInfo} changeText={this.changeText}></TableComponent>
            </div>
        )
    }
}

